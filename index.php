<!DOCTYPE html>
<html lang="es">
<?php include('red/bacanweb.php'); ?>
<?php include "head.php" ?>
<?php include "header.php" ?>  

<style>
    .item-product h5{
        font-weight: normal;
        font-size: 1.3em;
    }
    .item_price{
        font-size: 1.4em;
        font-weight: bold;
    }

    .metas{
        color: #00C292;
    }


    .footer-social{
    	border-top: none !important;
    }

    table, th, td{
    	border-top: none !important;
    }

    .table>thead>tr>th{
    	 border: none !important;
    }

    #data-table-basic_filter{
        float: right;
        margin-top: -32px;
    }


    .dataTables_filter label:after {
        content: '\e928';
        font-family: 'notika-icon';
        position: absolute;
        font-size: 14px;
        right: 9px;
        top: 8px;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current{
        padding: 7px 11px;
        line-height: 33px;
    }

    .dataTables_wrapper .dataTables_paginate {
        float: right;
        margin-top: -28px;
    }


</style>
<body>
	<div class="container">
		<div class="row">	
			<div class="col-sm-8 col-md-8 col-lg-9">
		        <div class="swiper-container" id="swiper-container">
		             <ul class="swiper-wrapper">
		                <li class="swiper-slide">
		                	<img src="data/option1/slide03.png" alt="">
		                    <div class="gt_banner_text gt_slide_1">
		                    </div>
		                </li>
		                <li class="swiper-slide">
		                   <img src="data/option1/slide02.png" alt="">
		                    <div class="gt_banner_text gt_slide_1">
		                    </div>
		                </li>
		             </ul>
		        </div>
			</div>
				<div class="col-sm-9 col-md-3">
					<div class="block-banner-right banner-hover">
						<a href="#"><img src="img/publicidad/publi01.png" alt="Banner"></a>
						<a href="#"><img src="img/publicidad/publi02.png" alt="Banner"></a>
					</div>
				</div>
		</div>
	</div>
	<div class="container">
		<div class="row">	
			<div class="col-sm-8 col-md-8 col-lg-9">


					<div class="block block-tabs">
						<div class="block-head">
							
							<ul class="nav-tab">                                   
		                        <li><a data-toggle="tab" href="#tab-2">All</a></li>
		                        <li  class="active"><a data-toggle="tab"  onclick="getProductos()">Productos</a></li>
		                        <li><a data-toggle="tab" onclick="getOfertas()">Servicios</a></li>
		                        <li><a data-toggle="tab" href="#tab-1">Restauran</a></li>
		                        <li><a data-toggle="tab" href="#tab-2">Paginas Web</a></li>
	                      	</ul>
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-1" class="tab-panel active">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>	
									




                        <table id="data-table-basic" class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php
                                        $query_select = "select id, id_pagina, nombre, descripcion, precio, 
                                                          cantidad, id_condicion, estatus, 
                                                          (select moneda from moneda where 
                                                          id=productos.id_moneda) as moneda,
                                                          imagen01,
                                                          (select nombre from condicion where
                                                          id=productos.id_condicion)condicion,
                                                          (select dominio from paginas where
                                                          id=productos.id_pagina)dominio,
                                                          (select id from estado_producto where
                                                          id=productos.id_estado_producto)estado
                                                          from productos where
                                                          estatus = 1
                                                          order by id desc";
                                        $query_execute = $mysqli->query($query_select); 
    
                                        while($query_productos = $query_execute->fetch_array()) {
                                        ?>  
                                            <tr>
                                                <td>
                    								<figure>
                    									<img src="paginas/images/<?php echo $query_productos["dominio"]?>/<?php echo $query_productos["imagen01"]?>" alt=""  />   
                    								</figure>		
                    					        </td> 	
                    					        <td>
                    		                        <div class="item-product">				
                    		                            <h5><?php echo $query_productos["nombre"]?></h5>
                    		                            <p><span class="item_price"><?php echo $query_productos["moneda"]?>
                    		                            <?php echo number_format($query_productos["precio"],2,",","."); ?></span></a></p>   
                                                        <p class="metas">Envios Gratis</p>  
                                                        <p><?php echo $query_productos["descripcion"]?></p>
                    		                        </div>
                               					</td>	
                                            </tr> 

                                                                            
                        
                                        <?php     
                                         }
                                        ?>  
                                </tbody>
                                    <tfoot>
                                        
                                    </tfoot>
                        </table>
  </ul>
						</div>


						
								<div id="tab-2" class="tab-panel">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										
									
									</ul>
								</div>


								
							</div>
						</div>
					</div>
				</div>

	

	


<?php include "footer.php" ?>

	

	<script src="js/js/jquery.js"></script>
    <script src="js/js/bootstrap.min.js"></script>
    <script src="js/js/jquery.bxslider.html"></script>
    <script src="js/js/jquery.accordion.js"></script>
    <script src="js/js/jquery.downCount.js"></script>
    <script src="js/js/jquery.prettyPhoto.js"></script>
    <script src="js/js/owl.carousel.js"></script>
    <script src="js/js/waypoints-min.js"></script>
    <script src="js/js/jquery-filterable.js"></script>
    <script src="js/js/wow.min.js"></script>
    <script src="js/js/custom.js"></script>
    <script src="js/js/swiper.jquery.min.js"></script>
    <script src="js/js/jquery.singlePageNav.js"></script>
    <script src="js/js/pluginse209.js"></script>


    <script src="js/jss/wow.min.js"></script>
    <script src="js/jss/jquery-price-slider.js"></script>
    <script src="js/jss/jquery.scrollUp.min.js"></script>
    <script src="js/jss/meanmenu/jquery.meanmenu.js"></script>
    <script src="js/jss/counterup/jquery.counterup.min.js"></script>
    <script src="js/jss/counterup/waypoints.min.js"></script>
    <script src="js/jss/counterup/counterup-active.js"></script>
    <script src="js/jss/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/jss/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/jss/sparkline/sparkline-active.js"></script>
    <script src="js/jss/wave/waves.min.js"></script>
    <script src="js/jss/wave/wave-active.js"></script>
    <script src="js/jss/plugins.js"></script>
    <script src="js/jss/data-table/jquery.dataTables.min.js"></script>
    <script src="js/jss/data-table/data-table-act.js"></script>

    
    <script>
        function getProductos() {
            $('#data-table-basic tbody').off('click');
                var table =$("#data-table-basic").DataTable({
                "destroy":true,
                "stateSave": true,
                "serverSide":false,
                "ajax":{
                    "method":"POST",
                    "url": "php/getProductos.php",
                    "dataSrc":""
                },
                "columns":[
                    {"data": "imagen01",
                        render : function(data, type, row) {
                            return '<figure><img src="paginas/images/'+row.dominio+'/'+data+'" alt=""  /></figure>   '
                        }
                    },
                    
                    {"data": "nombre",
                        render : function(data, type, row) {
                            return '<div class="item-product"> <h5>'+data+'</h5><p><span class="item_price">'+row.moneda+''+row.precio+'</span></a></p><p class="metas">Envios Gratis</p><p>'+row.descripcion+'></p></div>'
                        }
                    },
                ],
               
            });
        }



        function getOfertas() {
            $('#data-table-basic tbody').off('click');
                var table =$("#data-table-basic").DataTable({
                "destroy":true,
                "stateSave": true,
                "serverSide":false,
                "ajax":{
                    "method":"POST",
                    "url": "php/getOfertas.php",
                    "dataSrc":""
                },
                "columns":[
                    {"data": "imagen01",
                        render : function(data, type, row) {
                            return '<figure><img src="paginas/images/'+row.dominio+'/'+data+'" alt=""  /></figure>   '
                        }
                    },
                    
                    {"data": "nombre",
                        render : function(data, type, row) {
                            return '<div class="item-product"> <h5>'+data+'</h5><p><span class="item_price">'+row.moneda+''+row.precio+'</span></a></p><p class="metas">Envios Gratis</p><p>'+row.descripcion+'></p></div>'
                        }
                    },
                ],
               
            });
        }
    </script>

</body>
</html>