<!DOCTYPE html>
<html lang="es">
<?php include('../red/bacanweb.php'); ?>
<?php $id_pag = base64_decode($_REQUEST['id_pag']);  ?>
<?php include 'head.php'; ?>
<?php
$query_select = "select id, id_tipo_pagina, nombre, facebook, twitter, instagram, youtube,
                pinterest, whatsapp, android, logo, banner, mapa, dominio, estatus
                from paginas where id=$id_pag and estatus=1";
    $query_execute = $mysqli->query($query_select);
    $query_paginas = $query_execute->fetch_array(); 
    $facebook = $query_paginas["facebook"];
    $twitter = $query_paginas["twitter"];
    $instagram = $query_paginas["instagram"];
    $youtube = $query_paginas["youtube"];
    $pinterest = $query_paginas["pinterest"];
    $whatsapp = $query_paginas["whatsapp"];
    $android = $query_paginas["android"];
    $mapa = $query_paginas["mapa"];

$query_select = "select COUNT(id) as serv
                from servicio_pagina where id=$id_pag and estatus=1";
    $query_execute = $mysqli->query($query_select);
    $query_serv = $query_execute->fetch_array(); 
    $serv = $query_serv["serv"];

$query_select = "select id, nombre01, imagen01, pagina01, nombre02, imagen02, nombre03,
                imagen03, nombre04, imagen04, id_pagina
                from confi_paginas where id_pagina=$id_pag";
    $query_execute = $mysqli->query($query_select);
    $query_confi_pagina = $query_execute->fetch_array(); 
    $nombre01 = $query_confi_pagina["nombre01"];
    $imagen01 = $query_confi_pagina["imagen01"];
    $nombre02 = $query_confi_pagina["nombre02"];
    $imagen02 = $query_confi_pagina["imagen02"];
    $nombre03 = $query_confi_pagina["nombre03"];
    $imagen03 = $query_confi_pagina["imagen03"];
    $nombre04 = $query_confi_pagina["nombre04"];
    $imagen04 = $query_confi_pagina["imagen04"];
?>
<body>
<div class="gt_wrapper">
<?php include 'header.php'; ?>  
    <div class="gt_main_content_wrap">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-9">    
                        <div class="gt_team_detail default_width">
                          <?php
                $query_select = "select id, id_pagina, titulo, 
                cuerpo, estatus
                from texto_pagina where id_pagina= $id_pag and 
                estatus = 1";
                $query_execute = $mysqli->query($query_select); 
    
                    while($query_texto_pagina = $query_execute->fetch_array()) {
                            ?>  
                            <h5><?php echo $query_texto_pagina['titulo'];?></h5>
                            <p align="justify"><?php echo nl2br($query_texto_pagina['cuerpo'])?></p>
                    <?php     
                    }
                ?>
                    <?php 
                                if ($serv=="") { echo "";
                                    }else{ ?>
                                     <h5>Servicio</h5> 
                            <?php
                                }
                              ?> 
                        <?php
                $query_select = "select id, id_pagina, titulo, 
                valor, estatus
                from servicio_pagina where id_pagina= $id_pag and 
                estatus = 1";
                $query_execute = $mysqli->query($query_select); 
                    while($query_servicio_pagina = $query_execute->fetch_array()) {
                            ?>     
                            <div class="skill-content-3">
                                <div class="skill">
                                    <div class="progress">
                                        <div class="lead"><?php echo $query_servicio_pagina['titulo'];?></div>
                                        <div class="p_bg1 progress-bar wow fadeInLeft" data-progress="95%" style="width: <?php echo $query_servicio_pagina['valor'];?>%; visibility: visible; animation-duration: 1.5s; animation-delay: 1.2s; animation-name: fadeInLeft;" data-wow-duration="1.5s" data-wow-delay="1.2s"> <span><?php echo $query_servicio_pagina['valor'];?>%</span></div>
                                    </div>                                               
                                </div>
                            </div>
                             <?php     
                                        }
                                    ?>
                            <?php 
                                if ($mapa=="") { echo "";
                                    }else{ ?>
                                     <h5>Ubicación</h5> 
                            <?php
                                }
                              ?> 
                            <?php echo $query_paginas['mapa'];?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <aside class="gt_aside_outer_wrap">  
                            <div class="gt_aside_post_wrap gt_detail_hdg aside_margin_bottom">
                                <ul>
                                <?php
                                    $query_select = "select * from publicidad where estatus = 1";
                                    $query_execute = $mysqli->query($query_select); 
                    while($query_publicidad = $query_execute->fetch_array()) {
                            ?>    
                                    <li>
                                        <figure>
                                            <img src="../publicidad/<?php echo $query_publicidad["imagen"]?>" alt="">
                                        </figure>
                                        
                                    </li>
                               <?php     
                                  }
                                ?>      

                                </ul>
                            </div>
                            
                        </aside>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php include 'footer.php'; ?>  
</div>

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.bxslider.html"></script>
    <script src="../js/jquery.accordion.js"></script>
    <script src="../js/jquery.downCount.js"></script>
    <script src="../js/jquery.prettyPhoto.js"></script>
    <script src="../js/owl.carousel.js"></script>
    <script src="../js/waypoints-min.js"></script>
    <script src="../js/jquery-filterable.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/custom.js"></script>
    <script src="../js/swiper.jquery.min.js"></script>
    <script src="../js/jquery.singlePageNav.js"></script>
    <script src="../js/pluginse209.js"></script>
    <script type="text/javascript">

    if($('.gt_top3_menu').length){ 
        var stickyNavTop = $('.gt_top3_menu').offset().top;
        var stickyNav = function(){
            var scrollTop = $(window).scrollTop(); 
            if (scrollTop > stickyNavTop) { 
                $('.gt_top3_menu').addClass('gt_sticky');
            } else {
                $('.gt_top3_menu').removeClass('gt_sticky'); 
            }
        };
        stickyNav();
        $(window).scroll(function() {
            stickyNav();
        });
    }
    if($('.single-page').length){
        $('.single-page').singlePageNav({
            offset: $('.kode_pet_navigation').outerHeight(),
            filter: ':not(.external)',
            updateHash: true,
            beforeStart: function() {
                console.log('begin scrolling');
            },
            onComplete: function() {
                console.log('done scrolling');
            }
        });
    }
    </script>
</body>
</html>
