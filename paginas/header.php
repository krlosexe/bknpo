<header>
<div class="gt_top3_menu default_width single-page">
            <div class="container">
                <div >
                <center>
                    <a href="#"><img src="../paginas/images/<?php echo $query_paginas['dominio'];?>/<?php echo $query_paginas['logo'];?>" alt=""></a>
                </center>
                </div>
            </div>
        </div>
    </div>

<div class="gt_banner default_width">
        <div class="swiper-container" id="swiper-container">
             <ul class="swiper-wrapper">
                <li class="swiper-slide">
                    <img src="images/<?php echo $query_paginas['dominio'];?>/<?php echo $query_paginas['banner'];?>" alt="">
                    <div class="gt_banner_text gt_slide_1">
                    </div>
                </li>
             </ul>
         </div>
 </div>

 <section class="gt_about_bg" id="about-us">
            <div class="container">
              
                    
                       
                            
                            <div class="row">
                                <div class="col-md-3 col-sm-3 flip_container">
                                    <div class="gt_flip_effect">
                                        <div class="gt_about_icon">
                                            <a href=""><img src="header/<?php echo $query_confi_pagina["imagen01"]?>"></a>
                                        </div>
                                        <div class="gt_icon_des bg_1">
                                            <a href="<?php echo $query_confi_pagina["pagina01"]?>"><?php echo $query_confi_pagina["nombre01"]?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 flip_container">
                                    <div class="gt_flip_effect">
                                        <div class="gt_about_icon">
                                            <a href=""><img src="header/<?php echo $query_confi_pagina["imagen02"]?>"></a>
                                        </div>
                                        <div class="gt_icon_des bg_2">
                                            <a href="#"><?php echo $query_confi_pagina["nombre02"]?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 flip_container">
                                    <div class="gt_flip_effect">
                                        <div class="gt_about_icon">
                                            <a href=""><img src="header/<?php echo $query_confi_pagina["imagen03"]?>"></a>
                                        </div>
                                        <div class="gt_icon_des bg_3">
                                            <a href="#"><?php echo $query_confi_pagina["nombre03"]?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 flip_container">
                                    <div class="gt_flip_effect">
                                        <div class="gt_about_icon">
                                            <a href=""><img src="header/<?php echo $query_confi_pagina["imagen04"]?>"></a>
                                        </div>
                                        <div class="gt_icon_des bg_4">
                                            <a href="#"><?php echo $query_confi_pagina["nombre04"]?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </div>
        </section>

 <div class="social">
    <ul>

                            <?php 
                                if ($facebook=="") { echo "";
                                    }else{ ?>
                                     <li><a href="" class="icon-facebook" target="_blank"></a></li> 
                            <?php
                                }
                                
                                if ($twitter=="") { echo "";
                                    }else{ ?>
                                     <li><a href="" class="icon-twitter" target="_blank"></a></li>
                            <?php
                                }

                                if ($instagram=="") { echo "";
                                    }else{ ?>
                                    <li><a href="" class="icon-instagram" target="_blank"></a></li>    
                            <?php
                                }

                                if ($youtube=="") { echo "";
                                    }else{ ?>
                                    <li><a href="" class="icon-youtube" target="_blank"></a></li>
                            <?php
                                }

                                if ($pinterest=="") { echo "";
                                    }else{ ?>
                                    <li><a href="" class="icon-pinterest" target="_blank"></a></li>
                             <?php
                                }

                                if ($whatsapp=="") { echo "";
                                    }else{ ?>
                                    <li><a href="" class="icon-whatsapp" target="_blank"></a></li>
                            <?php
                                }

                                if ($android=="") { echo "";
                                    }else{ ?>
                                    <li><a href="" class="icon-android" target="_blank"></a></li>
                            <?php
                                }
                        ?>  
    </ul>
</div>
</header>