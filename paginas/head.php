<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Kid Template for Children and child.">
    <meta name="keywords" content="child,children,school,childcare,colorful">
    <meta name="author" content="BACAN Web">

    <title>BACAN Web</title>
    <!-- Swiper Slider CSS -->
    <link href="../css/swiper.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="../style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="../css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="../css/responsive.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">

  </head>