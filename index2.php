<!DOCTYPE html>
<html lang="es">
<?php include "head.php" ?>

<?php include "header.php" ?>  

	<!-- ./header -->
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-sm-3 col-md-2">
					<!-- Block vertical-menu -->
					<div class="block block-vertical-menu">
						<div class="vertical-head">
							<h5 class="vertical-title">Categories</h5>
						</div>
						<div class="vertical-menu-content">
	                        <ul class="vertical-menu-list">
	                            <li class="ef4896"><a href="#"><img class="icon-menu" alt="Funky roots" src="data/1.png">Fashion</a>
									<div class="vertical-dropdown-menu">
                                        <div class="vertical-groups col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="block-content-vertical-menu">
                                                        <h3 class="head">PRODUCTS SPECIAL</h3>
                                                        <div class="inner">
                                                            <p>Pellentesque in ipsum id orci porta dapibus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="row mega-products">
                                                        <div class="col-sm-4 mega-product">
                                                            <div class="product-avatar">
                                                                <a href="#"><img src="data/p10.jpg" alt="product1"></a>
                                                            </div>
                                                            <div class="product-name">
                                                                <a href="#">Cotton Lycra Leggings</a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="product-price">$139.98</span>
                                                                <span class="product-price-old">$169.00</span>
                                                            </div>
                                                            <div class="product-star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 mega-product">
                                                            <div class="product-avatar">
                                                                <a href="#"><img src="data/p11.jpg" alt="product1"></a>
                                                            </div>
                                                            <div class="product-name">
                                                                <a href="#">Cotton Lycra Leggings</a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="product-price">$139.98</span>
                                                                <span class="product-price-old">$169.00</span>
                                                            </div>
                                                            <div class="product-star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 mega-product">
                                                            <div class="product-avatar">
                                                                <a href="#"><img src="data/p12.jpg" alt="product1"></a>
                                                            </div>
                                                            <div class="product-name">
                                                                <a href="#">Cotton Lycra Leggings</a>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="product-price">$139.98</span>
                                                                <span class="product-price-old">$169.00</span>
                                                            </div>
                                                            <div class="product-star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
	                                </div>
	                            </li>
	                            <li class="e664fe">
                                    <a href="#"><img class="icon-menu" alt="Funky roots" src="data/2.png">Mother & Baby</a>
                                    <div class="vertical-dropdown-menu">
                                        <div class="vertical-groups">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="block-content-vertical-menu border banner-hover">
                                                        <a href="#"><img src="data/banner/b42.png" alt="Banner"></a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="block-content-vertical-menu banner-hover">
                                                        <a href="#"><img src="data/banner/b43.png" alt="Banner"></a>
                                                    </div>
                                                    <div class="block-content-vertical-menu">
                                                        <div class="inner">
                                                            <p>Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada. Proin eget tortor risus. </p>
                                                            <a href="#" class="button-radius">Shop now<span class="icon"></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="block-content-vertical-menu border-left">
                                                        <h3 class="head" style="background:#e664fe;">CATEGORIES</h3>
                                                        <div class="inner">
                                                        <ul class="vertical-menu-link">
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Skincare</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Metkup</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Mobile phone</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Tablet</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Men's Apparel</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Women's Apparel</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Watch sport</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Metkup</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Mobile phone</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Tablet</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
	                            <li class="fe64a9">
                                    <a href="#"><img class="icon-menu" alt="Funky roots" src="data/3.png">Cosmetics</a>
                                    <div class="vertical-dropdown-menu">
                                        <div class="vertical-groups">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="block-content-vertical-menu border">
                                                        <h3 class="head">CATEGORIES</h3>
                                                        <div class="inner">
                                                        <div class="inner-img">
                                                            <a href="#"><img src="data/banner/4.jpg" alt="Banner"></a>
                                                        </div>
                                                        <ul class="vertical-menu-link">
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Skincare</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Metkup</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Mobile phone</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Tablet</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="block-content-vertical-menu border">
                                                        <h3 class="head">CATEGORIES</h3>
                                                        <div class="inner">
                                                        <div class="inner-img">
                                                            <a href="#"><img src="data/banner/5.jpg" alt="Banner"></a>
                                                        </div>
                                                        <ul class="vertical-menu-link">
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Skincare</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Metkup</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Mobile phone</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Tablet</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="block-content-vertical-menu border">
                                                        <h3 class="head">CATEGORIES</h3>
                                                        <div class="inner">
                                                        <div class="inner-img">
                                                            <a href="#"><img src="data/banner/6.jpg" alt="Banner"></a>
                                                        </div>
                                                        <ul class="vertical-menu-link">
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Skincare</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Metkup</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Mobile phone</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Tablet</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="block-content-vertical-menu border">
                                                        <h3 class="head">CATEGORIES</h3>
                                                        <div class="inner">
                                                        <div class="inner-img">
                                                            <a href="#"><img src="data/banner/7.jpg" alt="Banner"></a>
                                                        </div>
                                                        <ul class="vertical-menu-link">
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Skincare</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Metkup</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Mobile phone</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="text">Tablet</span>
                                                                    <span class="count">(9)</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
	                            <li><a href="#"><img class="icon-menu" alt="Funky roots" src="data/4.png">Electronics</a></li>
	                            <li><a href="#"><img class="icon-menu" alt="Funky roots" src="data/5.png">Interior</a></li>
	                            <li>
	                            	<a class="parent" href="#"><img class="icon-menu" alt="Funky roots" src="data/6.png">Sport</a>
	                            </li>
	                            <li><a href="#"><img class="icon-menu" alt="Funky roots" src="data/7.png">Mobile & Tablet</a></li>
	                            <li><a href="#"><img class="icon-menu" alt="Funky roots" src="data/8.png">Other</a></li>
	                        </ul>
	                    </div>
					</div>
					<!-- ./Block vertical-menu -->
				</div>
				<!-- block search -->
	
				<!-- ./block search -->
				<!-- block cl-->
			
				<!-- ./block cl-->
				<div class="col-sm-9 col-md-7">
					<!-- Home slide -->
					<div class="block block-slider">
						<ul class="home-slider kt-bxslider">
							<li><img src="data/option2/slider1.jpg" alt="Slider"></li>
						</ul>
					</div>
					<!-- ./Home slide -->
				</div>
				<div class="col-sm-9 col-md-3">
					<div class="block-banner-right banner-hover">
						<a href="#"><img src="data/option2/banner1.png" alt="Banner"></a>
						<a href="#"><img src="data/option2/banner2.png" alt="Banner"></a>
					</div>
				</div>
				<!-- block banner owl-->
				<div class="col-sm-12">
					<div class="block block-banner-owl" >
						<div class="block-inner kt-owl-carousel" data-margin="30" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":3}}'>
							<div class="banner-text">
                                 <h4>TODAY'S</h4>
                                 <h2><b>TOP 50 EDO</b></h2>
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                                 <a class="button-radius white" href="#">Shop now<span class="icon"></span></a>
                             </div>
                             <div class="banner-text" style="background-color:#ffd900;">
                                 <h4>UPTO</h4>
                                 <h2><b>40% OFF</b></h2>
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                                 <a class="button-radius white" href="#">Shop now<span class="icon"></span></a>
                             </div>
                             <div class="banner-text" style="background-color:#fa8072;">
                                 <h4>EXTRA</h4>
                                 <h2><b>60% OFF</b></h2>
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                                 <a class="button-radius white" href="#">Shop now<span class="icon"></span></a>
                             </div>
						</div>
					</div>
				</div>
				<!-- ./block banner owl-->
				<!-- block tabs -->
				<div class="col-sm-12">
					<div class="block block-tabs">
						<div class="block-head">
							<div class="block-title">
								<div class="block-title-text text-lg">best selling</div>
							</div>
							<ul class="nav-tab">                                   
		                        <li><a data-toggle="tab" href="#tab-2">All</a></li>
		                        <li  class="active"><a data-toggle="tab" href="#tab-1">Beauty & Perfumes</a></li>
		                        <li><a data-toggle="tab" href="#tab-2">Mobile & Tablets</a></li>
		                        <li><a data-toggle="tab" href="#tab-1">Fashion</a></li>
		                        <li><a data-toggle="tab" href="#tab-2">Auto Accessories</a></li>
	                      	</ul>
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-1" class="tab-panel active">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p1.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p2.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p3.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p4.jpg" alt="Produuct"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p5.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div id="tab-2" class="tab-panel">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p6.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p7.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p8.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p9.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p10.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ./block tabs -->

				<!-- Block hot deals2 -->
				<div class="col-sm-12">
					<div class="block-hot-deals2">
						<h3 class="title">hot deals</h3>
						<div class="row">
							<div class="col-sm-4 col-md-3">
								<div class="hot-deal-tab">
									<div class="countdown">
										<span class="countdown-lastest" data-y="2016" data-m="10" data-d="1" data-h="00" data-i="00" data-s="00"></span>
									</div>
									<ul class="nav-tab">
				                        <li class="active"><a data-toggle="tab" href="#hotdeals-1">up to 70% off</a></li>
				                        <li><a data-toggle="tab" href="#hotdeals-2">up to 60% off</a></li>
				                        <li><a data-toggle="tab" href="#hotdeals-1">up to 50% off</a></li>
				                        <li><a data-toggle="tab" href="#hotdeals-2">up to 40% off</a></li>
			                      	</ul>
								</div>
							</div>
							<div class="col-sm-8 col-md-9">
								<div class="tab-container">
									<div id="hotdeals-1" class="tab-panel active">
										<ul class="products kt-owl-carousel" data-margin="30" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":3}}'>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p21.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star-half-o"></i>
					                                    </div>
					                                    <div class="product-button">
					                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
					                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
					                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
					                                    </div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p22.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star-half-o"></i>
					                                    </div>
					                                    <div class="product-button">
					                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
					                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
					                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
					                                    </div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p23.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star-half-o"></i>
					                                    </div>
					                                    <div class="product-button">
					                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
					                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
					                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
					                                    </div>
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div id="hotdeals-2" class="tab-panel">
										<ul class="products kt-owl-carousel" data-margin="30" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":3}}'>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p24.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star-half-o"></i>
					                                    </div>
					                                    <div class="product-button">
					                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
					                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
					                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
					                                    </div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p25.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star-half-o"></i>
					                                    </div>
					                                    <div class="product-button">
					                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
					                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
					                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
					                                    </div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p26.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star"></i>
					                                        <i class="fa fa-star-half-o"></i>
					                                    </div>
					                                    <div class="product-button">
					                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
					                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
					                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
					                                    </div>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Block hot deals2 -->
                <!-- block banner -->
                <div class="col-sm-12">
                    <div class="block block-banner2">
                        <div class="row">
                            <div class="box-left col-sm-12 col-md-8">
                                <div class="col-sm-6">
                                    <div class="inner">
                                        <h4><i>DIVE INTO NEW</i></h4>
                                        <h3><b>EXPERIENCES</b></h3>
                                        <div class="content-text">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry everything in-between</p>
                                        </div>
                                        <a href="#" class="button-radius">Shop now<span class="icon"></span></a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <a href="#"><img src="data/option2/br-banner1.jpg" alt="Banner"></a>
                                </div>
                            </div>
                            <div class="box-right col-sm-12 col-md-4">
                                <div class="item i1">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h5><i>DIVE INTO NEW</i></h5>
                                            <h5><b>EXPERIENCES</b></h5>
                                            <div class="content-text">
                                                <p>Clever additions that make your smartphone even smarter.</p>
                                            </div>
                                            <a href="#" class="button-radius">Shop now<span class="icon"></span></a>
                                        </div>
                                        <div class="col-sm-4">
                                            <a class="pull-right" href="#"><img src="data/option2/b8.png" alt="b8"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item i2" style="background: url('data/option2/b9.jpg') no-repeat right center;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5><i>DIVE INTO NEW</i></h5>
                                            <h5><b>EXPERIENCES</b></h5>
                                            <a href="#" class="button-radius">Shop now<span class="icon"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./block banner -->
				<!-- block tabs -->
				<div class="col-sm-12">
					<div class="block 18 block-tabs">
						<div class="block-head">
							<div class="block-title">
								<div class="block-title-text text-lg">new arrivals</div>
							</div>
							<ul class="nav-tab">                                   
		                        <li><a data-toggle="tab" href="#tab-4">All</a></li>
		                        <li  class="active"><a data-toggle="tab" href="#tab-3">Beauty & Perfumes</a></li>
		                        <li><a data-toggle="tab" href="#tab-4">Mobile & Tablets</a></li>
		                        <li><a data-toggle="tab" href="#tab-3">Fashion</a></li>
		                        <li><a data-toggle="tab" href="#tab-4">Auto Accessories</a></li>
	                      	</ul>
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-3" class="tab-panel active">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p11.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p12.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p13.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p14.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p15.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div id="tab-4" class="tab-panel">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p16.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p17.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p18.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p19.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p20.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ./block tabs -->

			</div>
		</div>
	</div>
    <div class="container">
        <div class="row">
            <div class="block-popular-cat2">
                <h3 class="title">Popular Categories</h3>
                <div class="block block-popular-cat2-item">
                    <div class="block-inner">
                        <div class="cat-name">Electronics</div>
                        <div class="box-subcat">
                            <ul class="list-subcat kt-owl-carousel" data-margin="0" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":7}}'>
                                <li class="item"><a href="#"><img src="data/option2/c1.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c2.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c3.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c4.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c5.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c6.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c7.jpg" alt="Cat"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="block block-popular-cat2-item box2">
                    <div class="block-inner">
                        <div class="cat-name">fashion</div>
                        <div class="box-subcat">
                            <ul class="list-subcat kt-owl-carousel" data-margin="0" data-nav="true" data-loop="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":7}}'>
                                <li class="item"><a href="#"><img src="data/option2/c8.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c9.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c10.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c11.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c12.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c13.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c14.jpg" alt="Cat"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="block block-popular-cat2-item box3">
                    <div class="block-inner">
                        <div class="cat-name">Sport</div>
                        <div class="box-subcat">
                            <ul class="list-subcat kt-owl-carousel" data-margin="0" data-nav="true" data-loop="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":7}}'>
                                <li class="item"><a href="#"><img src="data/option2/c15.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c16.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c17.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c18.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c19.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c20.jpg" alt="Cat"></a></li>
                                <li class="item"><a href="#"><img src="data/option2/c21.jpg" alt="Cat"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- footer -->
<?php include "footer.php" ?>
	<!-- ./footer -->
    <a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
	<script type="text/javascript" src="lib/jquery/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="lib/owl.carousel/owl.carousel.min.js"></script>
	<script type="text/javascript" src="lib/jquery-ui/jquery-ui.min.js"></script>
	<!-- COUNTDOWN -->
	<script type="text/javascript" src="lib/countdown/jquery.plugin.js"></script>
	<script type="text/javascript" src="lib/countdown/jquery.countdown.js"></script>
	<!-- ./COUNTDOWN -->
	<script type="text/javascript" src="js/jquery.actual.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</body>

<!-- Mirrored from kute-themes.com/html/edo/html/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Mar 2019 00:52:45 GMT -->
</html>